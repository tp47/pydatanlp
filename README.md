# pydatanlp

PyData meetup @ HolidayCheck on 31st July

Setup:
1. Clone this repo
2. `python3 -m venv venv`
3. `source venv/bin/activate`
3. `pip install spacy`
4. `pip install tabulate`
5. `python -m spacy download en_core_web_sm`

For working with German:
`python -m spacy download de_core_web_sm`

For word similarity:
`python -m spacy download en_core_web_md`

To start the notebook:
`jupyter notebook`